#!/bin/bash

echo "[`date`] Debug ON "
set -eu -o pipefail

cfg_dir=$HOME/.config
bin_dir=$HOME/.local/bin
pic_dir=%HOME/pictures

rm $HOME/.config/todo/config

echo "[`date`] Copy config files"
cp -r config/ $cfg_dir/

echo "[`date`] Copy bin files"
cp -r bin/ $bin_dir/

echo "[`date`] Copy images "
cp background.jpg lock.png $pic_dir/

echo "[`date`] Copy ZSHRC"
cp .zshrc $HOME/

echo "[`date`] Change to ZSH"
chsh -s /usr/bin/zsh
