# Deva dotfiles

** Under construction **

## Introduction

These are my dot files with configurations and binaries for i3wm and related applications. There is also my personal post-installation script for OS Gecko Linux "Barebones" an OpenSuse Thumbleweed spin.

This was only possible with the help of videos, forums and the documentation of each application, thank you very much and especially the following references:
[Manual do Código] (https://www.youtube.com/@ManualdoCodigo)
[Gabriel TWM] (https://www.youtube.com/@TWMLinuxGabriel)
[Debxp] (https://www.youtube.com/@debxp)
[Nerd Fonts] (https://www.nerdfonts.com/cheat-sheet)
[Color Name] (https://www.color-name.com/)

## Screenshot
Soon

## What does ```post_install_pt1.sh``` do?

* create .config directory and subdirectories
* create bin and source directories
* rename user directories
* set user_dirs.dirs to lowercase user directory names
* remove Google Chrome repositories
* update the system
* restart the system

## What does ```post_install_pt3.sh``` do?

* check sudo privileges
* install the necessary applications for a good i3wm
* change sh to zsh

## What does ```post_install_pt3.sh``` do?

* add flathub repository
* install todotxt-cli
* ~~install zscroll~~
* install todofi
* install Hack Nerd Fonts
* install polybar scripts (modules)
* install neovim plugin
* install zsh 10k power level plugin
* install zsh-autosuggestions zsh plugins
* install greenclip clipboard manager for rofi
* change script mode to executable (chmod+x)
* ~~install flatpak apps~~
* ~~create symlinks to installed flatpak apps~~

## General information

* WM: i3
* bar: polybar
* Notification: dunst
* Menu: dmenu/rofi
* Terminal: kitty
* Editor: neovim
* SH: zsh
* Background manager: feh
* ~~Multi-language runtime version manager: asdf~~
* File manager: ranger
* Player: mplayer
* PDF view: mupdf
* ~~Virtualization: qemu~~
* Screenshot: flameshot
* Lockscreen: i3lock
