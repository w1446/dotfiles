#!/bin/bash

### SCRITP THAT INSTALL SOFTWARES WITH SUDO ###
echo "[`date`] Debug ON "
set -eu -o pipefail

echo "[`date`] Check sudo "
sudo -n true
test $? -eq 0 || exit 1 "you should have sudo privilege to run this script"

echo "[`date`] Installing 32 packages. i3wm, polybar, dunst, rofi, kity, picom, feh, zsh, git, flatpak, wget, curl, ... "
while read -r p ; do zypper install -y $p ; done < <(cat << "EOF"
	i3
	i3lock
	i3status
	dmenu
	polybar
	dunst
	rofi
	picom
	kitty
	feh
	numlockx
	neovim
	git
	zsh
	pavucontrol
	mplayer
	mupdf
	flatpak
	htop
	wget
	curl
	ImageMagick
	ranger
	man
	geany
	jq
	make
	playerctl
	nodejs19
	npm19
	ripgrep
	fd
EOF
)
#Maibe add this packages
	#xclip or xset
	#cargo
	#qutebrowser
#echo "[`date`] Intall base devel "
#zypper install -y -t pattern devel_basis


echo "[`date`] Reboot the system"
reboot
