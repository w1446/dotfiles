#!/bin/bash

echo "[`date`] Debug ON "
set -eu -o pipefail

bin_dir=$HOME/.local/bin
flatpak_dir=/var/lib/flatpak/exports/bin
cfg_dir=$HOME/.config

echo "[`date`] Add Flathub repo to Flatpak"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

echo "[`date`] Manual install todotxt-cli"
cd /tmp/
git clone https://github.com/todotxt/todo.txt-cli.git
cd todo.txt-cli/
make
sudo make install CONFIG_DIR=$cfg_dir INSTALL_DIR=$bin_dir BASH_COMPLETION=/usr/share/bash-completion/completions

echo "[`date`] zscroll for spotify"
cd /tmp/
git clone https://github.com/noctuid/zscroll
cd /zscroll/
sudo python3 setup.py install

echo "[`date`] Install todofi.sh"
cd /tmp/
git clone https://github.com/hugokernel/todofi.sh.git
mv todofi.sh/todofi.sh $bin_dir/

echo "[`date`] Get HACK font and install"
cd /tmp/
wget -c https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/Hack.zip
unzip Hack.zip
rm *Windows*
cp *.ttf $HOME/.local/share/fonts/
fc-cache -f -v

echo "[`date`] Polybar Modules/Scripts"
cd /tmp/ 
git clone https://github.com/polybar/polybar-scripts.git
echo " battery combined udev "
sudo mv polybar-scripts/polybar-scripts/battery-combined-udev/95-battery.rules /etc/udev/rules.d/
echo " system usb udev "
mv polybar-scripts/polybar-scripts/system-usb-udev/system-usb-udev.sh $bin_dir/
sudo mv polybar-scripts/polybar-scripts/system-usb-udev/95-usb.rules /etc/udev/rules.d/
echo " updates flatpak "
mv polybar-scripts/polybar-scripts/updates-flatpak/updates-flatpak.sh $bin_dir/
echo " spotify now-playing "
git clone https://github.com/PrayagS/polybar-spotify.git
mv polybar-spotify/get_spotify_status.sh $bin_dir/
mv polybar-spotify/scroll_spotify_status.sh $bin_dir/

echo "[`date`] Install NeoVim Plug"
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

echo "[`date`] Install PowerLevel10k plugin"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $cfg_dir/zsh/plugins/powerlevel10k

echo "[`date`] Install ash-autosuggestions plugin"
git clone https://github.com/zsh-users/zsh-autosuggestions $cfg_dir/zsh/plugins/zsh-autosuggestions

#echo "[`date`] Install asdf"
#git clone https://github.com/asdf-vm/asdf.git $cfg_dir/asdf --branch v0.10.2
#cat "$cfg_dir/asdf/asdf.sh" >> $HOME/.zshrc

echo "[`date`] Install greenclip clipboard manager for rofi"
cd $bin_dir
wget https://github.com/erebe/greenclip/releases/download/v4.2/greenclip

echo "[`date`] Change mode to executable for scripts in ~/.local/bin"
cd $bin_dir
chmod +x *

############ TODO ################
# GET Volume
# GET rofi-power (EndeavourOS)

echo "Install flatpak apps"
flatpak install com.spotify.Client org.libreoffice.LibreOffice org.flameshot.Flameshot com.microsoft.Edge com.authy.Authy org.keepassxc.KeePassXC com.discordapp.Discord net.cozic.joplin_desktop org.mozilla.Thunderbird com.github.eneshecan.WhatsAppForLinux org.telegram.desktop -y

echo "Links simbólicos para acessar via linha de comando"
ln -s $flatpak_dir/com.spotify.Client $bin_dir/spotify
ln -s $flatpak_dir/org.flameshot.Flameshot $bin_dir/flameshot
ln -s $flatpak_dir/com.microsoft.edge $bin_dir/edge
ln -s $flatpak_dir/org.libreoffice.LibreOffice $bin_dir/loffice
ln -s $flatpak_dir/com.authy.Authy $bin_dir/authy
ln -s $flatpak_dir/org.keepassxc.KeePassXC $bin_dir/kpass
ln -s $flatpak_dir/com.discordapp.Discord $bin_dir/discord
ln -s $flatpak_dir/net.cozic.joplin_desktop $bin_dir/joplin
ln -s $flatpak_dir/org.mozilla.Thunderbird $bin_dir/thunderbird
ln -s $flatpak_dir/com.github.eneshecan.WhatsAppForLinux $bin_dir/wpp
ln -s $flatpak_dir/org.telegram.desktop $bin_dir/telegram

