" Themes configurations
"
if has('+termguicolors')
    set termguicolors
endif

let g:sonokai_style='espresso'
let g:sonokai_diagnostic_line_highlight=1
let g:sonokai_better_performance=1

colorscheme sonokai
